const fs = require('fs');
var file = "results.json";
if(process.argv[0].endsWith(".json")) {
    file = process.argv[0];
}
var fileData = fs.readFileSync(file);
var results = JSON.parse(fileData);
console.log("All Issues Found:")
for (var i=0; i<results.results.TestResults.Issues.Issue.length; i++){
    var Issue = results.results.TestResults.Issues.Issue[i];
    console.log(`CWE-${Issue.CWEId}: ${Issue.IssueType}: ${Issue.Files.SourceFile.File}:${Issue.Files.SourceFile.Line}`)
}