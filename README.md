# README #
* node is required (tested against v15.14.0)

Download the parse.js script or clone the repo and copy the script to your workfolder

Run with:

`node parse.js`

When running the pipeline scanner it by default creats a results.json file, this is what the script is looking for unless you pass in a filename:

`node parse.js file.json`

Output example with included results.json
```bash
====================
Analysis Successful!
====================

==================
Analyzed 8 issues.
==================
-------------------------------------
Skipping 8 issues of Medium severity.
-------------------------------------

==================================
SUCCESS: No issues passed filters!
==================================

$node parse.js

All Issues Found:
CWE-352: Cross-Site Request Forgery (CSRF): cwe-73/test.js:2
CWE-312: Cleartext Storage of Sensitive Information: cwe-73/test.js:12
CWE-312: Cleartext Storage of Sensitive Information: cwe-73/test.js:14
CWE-312: Cleartext Storage of Sensitive Information: cwe-73/test.js:15
CWE-117: Improper Output Neutralization for Logs: cwe-73/test.js:42
CWE-117: Improper Output Neutralization for Logs: cwe-73/test.js:43
CWE-80: Improper Neutralization of Script-Related HTML Tags in a Web Page (Basic XSS): cwe-73/test.js:44
CWE-117: Improper Output Neutralization for Logs: cwe-73/test.js:48
```

### What is this repository for? ###

This is a basic Veracode pipeline-scan results parser intended to print out the entire results to the console including the filtered ones.



